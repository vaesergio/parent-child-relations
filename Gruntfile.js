module.exports = function (grunt) {

  grunt.initConfig({
    concat: {
      bowerjs: {
        src: [
                    './bower_components/angular/angular.js',
                    './bower_components/angular-animate/angular-animate.js',
                    './bower_components/angular-aria/angular-aria.js',
                    './bower_components/angular-resource/angular-resource.js',
                    './bower_components/angular-material/angular-material.js',
                    './bower_components/underscore/underscore.js',
                    './bower_components/angular-ui-router/release/angular-ui-router.js',
                ],
        dest: './public/bower.js'
      },
      scriptsjs: {
        src: [
                    './source/app.js',
                    './source/factory/myFactory.js',
                    './source/**/*.js'
                ],
        dest: './public/script.js'
      },
      bowercss: {
        src: [
                    './bower_components/angular-material/angular-material.css'

                ],
        dest: './public/bower.css'
      },
      customcss: {
        src: [
                    './source/css/**/*.css'
                ],
        dest: './public/style.css'
      }
    },
    uglify: {
      bowerjs: {
        files: {
          './public/bower.min.js': './public/bower.js'
        }
      },
      scriptsjs: {
        files: {
          './public/script.min.js': './public/script.js'
        }
      }
    },
    cssmin: {
      bowercss: {
        files: {
          './public/bower.min.css': './public/bower.css'
        }
      },
      customcss: {
        files: {
          './public/style.min.css': './public/style.css'
        }
      }
    },
    copy: {
      html: {
        cwd: './source/',
        src: '**/*.html',
        dest: './public/',
        expand: true
      }
    },
    //    watch: {
    //
    //      gruntfile: {
    //        files: ['Gruntfile.js'],
    //        tasks: ['default'],
    //        options: {
    //          livereload: 35333,
    //          interval: 500
    //        }
    //      },
    //      scripts: {
    //        files: ['./source/**/*.js'],
    //        tasks: ['concat:scriptsjs', 'uglify:scriptsjs'],
    //        options: {
    //          livereload: 35333,
    //          interval: 500
    //        }
    //      },
    //      css: {
    //        files: ['./source/css/**/*.css'],
    //        tasks: ['concat:customcss', 'cssmin:customcss'],
    //        options: {
    //          livereload: 35333,
    //          interval: 500
    //        }
    //      },
    //      html: {
    //        files: ['./source/**/*.html'],
    //        tasks: ['copy:html'],
    //        options: {
    //          livereload: 35333,
    //          interval: 50
    //        }
    //      }
    //    }
  });

  //Loading NPM tasks
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  //grunt.loadNpmTasks('grunt-contrib-watch');

  //default task
  grunt.registerTask('default', ['concat', 'uglify', 'cssmin', 'copy']);
};