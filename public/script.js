angular.module('myApp', ['ngResource',
                         'ngMaterial',
                         'ui.router'])

.config(['$stateProvider', function ($stateProvider) {

  $stateProvider
    .state('home', {
      url: '',
      templateUrl: './html/partial.html',
      controller: 'myController',
      controllerAs: 'ctrl',
      resolve: {
        parents: ['Categories', function (Categories) {
          return Categories.get();
          }]
      }
    });
}]);
angular.module('myApp')
  .factory('Categories', ['$resource', function ($resource) {
    return $resource('/categories/:id', {
      id: '@id'
    }, {
      findParent: {
        method: 'GET',
        url: '/categories-event/:id'
      },
      get: {
        method: 'GET',
        url: '/categories',
        isArray: true,
      }
    });
  }]);
angular.module('myApp')
  .controller('myController', ['Categories', 'Underscore', function (Categories, _) {

    var ctrl = this;

    ctrl.categories = [];
    ctrl.newCategory = new Categories();

    ctrl.findParent = function (category) {
      var findcategoryId = _.find(ctrl.categories, function (data) {
        if (category === data.category)
          return data.id;
      });
      Categories.findParent({
        id: findcategoryId.id
      }).$promise.then(function (result) {
        if (result.id === findcategoryId.id)
          ctrl.oldest = findcategoryId.category + " has no parents";
        else
          ctrl.oldest = "The oldest parent of " + findcategoryId.category + " is " + result.category;
        ctrl.newCategory = new Categories();
      }).catch(function (err) {
        ctrl.test = err;
      });
    };

    ctrl.addNew = function () {
      if ((ctrl.newCategory.category && ctrl.newCategory.categoryId) &&
        (ctrl.newCategory.category !== ctrl.newCategory.categoryId)) {

        var findCategory = _.find(ctrl.categories, function (data) {
          return data.category === ctrl.newCategory.categoryId;
        });

        if (findCategory) {

          if (!ctrl.findSubCat(ctrl.newCategory.category)) {

            ctrl.newCategory.categoryId = findCategory.id;

            ctrl.newCategory.$save().then(function (data) {
              ctrl.categories.push(data);
              ctrl.newCategory = new Categories();
            });
          } else {
            alert("Please, choose another category name. It already exists");
          }
        } else {

          if (!ctrl.findSubCat(ctrl.newCategory.category)) {

            var category = new Categories();
            category.category = ctrl.newCategory.categoryId;
            category.$save().then(function (data) {
              ctrl.categories.push(data);
              ctrl.newCategory.categoryId = data.id;
              ctrl.newCategory.$save().then(function (data) {
                ctrl.categories.push(data);
                ctrl.newCategory = new Categories();
              }).catch(function (err) {
                console.error(err);
              });
            }).catch(function (err) {
              console.error(err);
            });
          } else {
            alert("Please, choose another subcategory name. It already exists");
          }
        }
      } else {
        alert("The fields are empty or their names are the same. Please check your fields.");
      }

    };

    ctrl.findSubCat = function (category) {
      var result = _.find(ctrl.categories, function (data) {
        if (data.category === category)
          return data.id;
      });
      return result;
    };

      }]);

(function () {
  'use strict';

  angular.module('myApp')
    .factory('Underscore', Underscore);

  Underscore.$inject = ['$window'];

  function Underscore($window) {
    return $window._;
  }

})();