var Promise = require("bluebird");

//console.log('query === ' + JSON.stringify(query));
//console.log('parts === ' + parts);

// id
var id = parts[0];

var findParent = function (id) {
  return new Promise(function (resolve, reject) {
    dpd.categories.get({
      id: id
    }).then(function (data) {
      if (data.categoryId) {
        return findParent(data.categoryId).then(resolve).catch(reject);
      } else {
        resolve(data);
      }
    }).catch(reject);
  });
};

$addCallback();

findParent(id).then(function (result) {
  setResult(result);
  $finishCallback();
}).catch(function (err) {
  if (err) {
    console.log(err);
    cancel('Unexpected', 500);
  }
});