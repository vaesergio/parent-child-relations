angular.module('myApp')
  .factory('Categories', ['$resource', function ($resource) {
    return $resource('/categories/:id', {
      id: '@id'
    }, {
      findParent: {
        method: 'GET',
        url: '/categories-event/:id'
      },
      get: {
        method: 'GET',
        url: '/categories',
        isArray: true,
      }
    });
  }]);