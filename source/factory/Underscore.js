(function () {
  'use strict';

  angular.module('myApp')
    .factory('Underscore', Underscore);

  Underscore.$inject = ['$window'];

  function Underscore($window) {
    return $window._;
  }

})();