angular.module('myApp', ['ngResource',
                         'ngMaterial',
                         'ui.router'])

.config(['$stateProvider', function ($stateProvider) {

  $stateProvider
    .state('home', {
      url: '',
      templateUrl: './html/partial.html',
      controller: 'myController',
      controllerAs: 'ctrl',
      resolve: {
        parents: ['Categories', function (Categories) {
          return Categories.get();
          }]
      }
    });
}]);